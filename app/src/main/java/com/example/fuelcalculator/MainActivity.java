package com.example.fuelcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    static final boolean VERBOSE = true;

    static int    ceilRaceLaps = 0;
    static double raceLaps = 0;
    static double raceLength = 0;
    static double lapLength = 0;
    static double litresLap = 0;
    static double margin = 0;

    static String totalLapsString;
    static String totalFuelString;
    static String totalFuelNoMarginString;
    static String fuelUnitsString;
    static String defaultFuelString;
    static String defaultLapsString;

    public void computeLaps() {
        raceLaps = (raceLength / lapLength);
        ceilRaceLaps = (int) Math.ceil(raceLaps);
    }

    public double computeFuelNoMargin() {
        return raceLaps * litresLap;
    }

    public double computeFuel() {
        return raceLaps * litresLap * (1 + margin / 100.0);
    }

    public void showDefaultFuel () {
        Locale current = getResources().getConfiguration().locale;

        TextView totalFuel = findViewById(R.id.TotalFuel);
        totalFuel.setText(String.format(current, "%s %s%s", totalFuelString, defaultFuelString, fuelUnitsString));

        TextView totalFuelNoMargin = findViewById(R.id.TotalFuelNoMargin);
        totalFuelNoMargin.setText(String.format(current, "%s %s%s", totalFuelNoMarginString, defaultFuelString, fuelUnitsString));

        TextView totalLaps = findViewById(R.id.TotalLaps);
        totalLaps.setText(String.format(current, "%s %s (%s)", totalLapsString, defaultLapsString, defaultLapsString));
    }

    public void recomputeFuel() {
        if (lapLength == 0) {
            return;
        }

        computeLaps();
        double fuelNoMargin = computeFuelNoMargin();
        double fuelMargin = computeFuel();

        Locale current = getResources().getConfiguration().locale;

        TextView totalFuel = findViewById(R.id.TotalFuel);
        totalFuel.setText(String.format(current, "%s %.2f%s", totalFuelString, fuelMargin, fuelUnitsString));

        TextView totalFuelNoMargin = findViewById(R.id.TotalFuelNoMargin);
        totalFuelNoMargin.setText(String.format(current, "%s %.2f%s", totalFuelNoMarginString, fuelNoMargin, fuelUnitsString));

        TextView totalLaps = findViewById(R.id.TotalLaps);
        totalLaps.setText(String.format(current, "%s %d (%.1f)", totalLapsString, ceilRaceLaps, raceLaps));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        totalFuelString = getString(R.string.total_fuel);
        totalFuelNoMarginString = getString(R.string.total_litres_no_margin);
        fuelUnitsString = getString(R.string.litres_abbrv);
        defaultFuelString = getString(R.string.default_fuel);
        totalLapsString = getString(R.string.total_laps);
        defaultLapsString = getString(R.string.default_laps);

        showDefaultFuel();

        final EditText raceLengthET = findViewById(R.id.RaceLengthVal);
        raceLengthET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do
                if (!s.equals("")) {
                    try {
                        raceLength = 60 * Double.parseDouble(raceLengthET.getText().toString());
                        recomputeFuel();
                    } catch (Exception e) {
                        if (VERBOSE) {
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(getApplicationContext(), "Cannot parse string: " + s, duration);
                            toast.show();
                        }
                        showDefaultFuel();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        final EditText lapLengthET = findViewById(R.id.LapTimeVal);
        lapLengthET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do
                if (!s.equals("")) {
                    try {
                        lapLength = Double.parseDouble(lapLengthET.getText().toString());
                        recomputeFuel();
                    } catch (Exception e) {
                        if (VERBOSE) {
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(getApplicationContext(), "Cannot parse string: " + s, duration);
                            toast.show();
                        }
                        showDefaultFuel();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        final EditText litresLapET = findViewById(R.id.FuelLapVal);
        litresLapET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do
                if (!s.equals("")) {
                    try {
                        litresLap = Double.parseDouble(litresLapET.getText().toString());
                        recomputeFuel();
                    } catch (Exception e) {
                        if (VERBOSE) {
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(getApplicationContext(), "Cannot parse string: " + s, duration);
                            toast.show();
                        }
                        showDefaultFuel();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        final EditText marginET = findViewById(R.id.MarginVal);
        marginET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do
                if (!s.equals("")) {
                    try {
                        margin = Double.parseDouble(marginET.getText().toString());
                        recomputeFuel();
                    } catch (Exception e) {
                        if (VERBOSE) {
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(getApplicationContext(), "Cannot parse string: " + s, duration);
                            toast.show();
                        }
                        showDefaultFuel();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}
